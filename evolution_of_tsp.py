import random
import networkx as nx
import matplotlib.pyplot as plt
from copy import deepcopy


random.seed(12)

NODES = [10, 20, 50, 100]


def generate_full_graph_with_distances(nodes):
    G = nx.complete_graph(nodes)
    for (u, v, w) in G.edges(data=True):
        w['distance'] = random.randint(1, 499)
    return G

def calculate_individual_fitness(G, p):
    distance = 0
    for i in range(len(p)-1):
        distance += G.edges[p[i], p[i+1]]['distance']
    score = 1/distance
    return score

def generate_starting_population(G, population_size=50, start_node=0):
    P = []
    x = [i for i in range(0, len(G.nodes)) if i != start_node]
    for i in range(population_size):
        random.shuffle(x)
        individual = [start_node] + x + [start_node] 
        P.append({
            'individual': individual, 
            'score': calculate_individual_fitness(G, individual)
            })
    return sorted(P, key=lambda x: x['score'], reverse=True)

def crossover(p1, p2):
    '''Crossover is represented by ordered 1 crossover operator.'''
    a = random.randint(1, len(p1)-2) 
    b = random.randint(a + 1, len(p1)-1)
    offspring = [i for i in p2 if i not in p1[a:b]]
    offspring[a:a] = p1[a:b]
    return offspring

def mutate(p):
    '''Mutation is represented by a simple swap operator.'''
    a, b = random.sample(range(1, len(p)-1), k=2)
    p[a], p[b] = p[b], p[a]

def roulette_wheel_selection(G, P, population_size, elitism_factor, start_node):
    e = round(population_size*elitism_factor)
    cum_score = sum([p['score'] for p in P[e:]])
    weight_distribution = [p['score']/cum_score for p in P[e:]]
    P_new = P[:e] + random.choices(P[e:], weights=weight_distribution, k=(population_size-e))
    return sorted(P_new, key=lambda x: x['score'], reverse=True)

def next_generation(G, P, population_size=50, elitism_factor=0.1, mutation_coefficient=0.01, crossover_coefficient=0.6, start_node=0):
    for i in range(len(P)):
        p1, p2 = random.sample(range(len(P)), k=2)
        if random.random() <= crossover_coefficient: 
            individual = crossover(P[p1]['individual'], P[p2]['individual'])
            P.append({
                'individual': individual,
                'score': calculate_individual_fitness(G, individual)
            })

    for p in P:
        if random.random() <= mutation_coefficient:
            mutate(p['individual'])
            p['score'] = calculate_individual_fitness(G, p['individual'])

    P = sorted(P, key=lambda x: x['score'], reverse=True)
    return roulette_wheel_selection(G, P, population_size, elitism_factor, start_node)

def gen_x(G, P, x, population_size=50, elitism_factor=0.1, mutation_coefficient=0.01, crossover_coefficient=0.6):
    print('Generating population #{}, with parameters:\npopulation_size: {}\nelitism_factor: {}\nmutation_coefficient: {}\ncrossover_coefficient: {}'.format(x, population_size, elitism_factor, mutation_coefficient, crossover_coefficient))
    for i in range(x):
        P = next_generation(G, P, population_size, elitism_factor, mutation_coefficient, crossover_coefficient)
    best_route(P)

def best_route(P):
    print('individual: {}'.format(P[0]))
    print('distance: {}'.format(round(1/P[0]['score'])))
    print()

def main():
    G = generate_full_graph_with_distances(7)
    P = generate_starting_population(G, 10, 2)

    gen_x(G, deepcopy(P), 0, 50, 0.1, 0.02, 0.4)
    gen_x(G, deepcopy(P), 10, 50, 0.1, 0.02, 0.4)
    gen_x(G, deepcopy(P), 50, 50, 0.1, 0.02, 0.4)
    gen_x(G, deepcopy(P), 100, 50, 0.1, 0.02, 0.4)
    gen_x(G, deepcopy(P), 500, 50, 0.1, 0.02, 0.4)
    gen_x(G, deepcopy(P), 1000, 50, 0.1, 0.02, 0.4)

    pos = nx.spring_layout(G)
    nx.draw(G, pos, node_size=700)
    nx.draw_networkx_labels(G, pos, font_size=20, font_family='sans-serif')
    edge_labels = nx.get_edge_attributes(G, 'distance')
    nx.draw_networkx_edge_labels(G, pos, label_pos=0.3, edge_labels = edge_labels)
    plt.show()


if __name__ == '__main__':
    main()
